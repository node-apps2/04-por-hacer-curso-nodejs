const argv = require("./config/yargs").argv;
const { crear, leer, actualizar, borrar } = require("./por-hacer/por-hacer");
const colors = require("colors");
let comando = argv._[0];

switch (comando) {
  case "crear":
    let tarea = crear(argv.descripcion);
    console.log(tarea);
    break;
  case "listar":
    let listado = leer();

    for (let tarea of listado) {
      console.log("=============".green);
      console.log(tarea.descripcion);
      console.log("Estado :", tarea.completado);
      console.log("=============".green);
    }
    break;
  case "actualizar":
    let resp = actualizar(argv.descripcion, argv.completado);
    console.log(resp);
    break;
  case "borrar":
    let response = borrar(argv.descripcion);
    console.log(response);
    break;
  default:
    console.log("comando no reconocido");
    break;
}
