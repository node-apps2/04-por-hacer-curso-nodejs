let descripcion = {
  demand: true,
  alias: "d",
  desc: "Descripcion de la tarea por hacer"
};

let completado = {
  default: true,
  alias: "c",
  desc: "Marca como completado o pendiente la tarea"
};

const argv = require("yargs")
  .command("crear", "Crear elemento por hacer", {
    descripcion
  })
  .command("actualizar", "Actualiza el estado completo de una tarea", {
    descripcion,
    completado
  })
  .command("borrar", "Borrar el elemento por hacer", {
    descripcion
  })
  .help().argv;

module.exports = { argv };
