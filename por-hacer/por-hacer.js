const fs = require("fs");

let listado_por_hacer = [];

const guardar = () => {
  let data = JSON.stringify(listado_por_hacer);
  fs.writeFile("./db/data.json", data, (err, fd) => {
    if (err) throw err;
    console.log("archivo grabado");
  });
};

const leer = () => {
  let listado = [];
  try {
    listado = require("../db/data");
  } catch (error) {
    listado = [];
  }
  return listado;
};

const crear = descripcion => {
  listado_por_hacer = leer();

  let porHacer = {
    descripcion: descripcion,
    completado: false
  };
  listado_por_hacer.push(porHacer);
  guardar();
  return porHacer;
};

const actualizar = (desp, completado = true) => {
  listado_por_hacer = leer();

  let index = listado_por_hacer.findIndex(tarea => tarea.descripcion === desp);
  if (index >= 0) {
    listado_por_hacer[index].completado = completado;
    guardar();
    return true;
  } else {
    return false;
  }
};

const borrar = desp => {
  let lista = leer();

  let index = lista.findIndex(tarea => tarea.descripcion === desp);
  if (index === -1) {
    return false;
  } else {
    lista.splice(index, 1);
    listado_por_hacer = lista;
    guardar();
    return true;
  }
};

module.exports = { crear, leer, actualizar, borrar };
